module gitlab.com/gfxlabs/gfxsvg

go 1.17

require (
	github.com/srwiley/rasterx v0.0.0-20210519020934-456a8d69b780
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410
	golang.org/x/net v0.0.0-20220121210141-e204ce36a2ba
)

require golang.org/x/text v0.3.7 // indirect
